package com.kotlinexample.presentation.presenter.login


import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kotlinexample.api.ApiLoginService.Factory.create
import com.kotlinexample.api.LoginModel
import com.kotlinexample.api.RegisterModel
import com.kotlinexample.api.UserModel
import com.kotlinexample.presentation.view.login.LoginView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class LoginPresenter : MvpPresenter<LoginView>(){

    private var apiLoginCall: Call<UserModel>? = null

    fun login(loginModel: LoginModel) {
        apiLoginCall = create().login(loginModel)
        apiLoginCall?.enqueue(object : retrofit2.Callback<UserModel> {
            override fun onResponse(call: Call<UserModel>?, response: Response<UserModel>?) {
                viewState.login(response?.body() ?: UserModel(false, ""))
            }

            override fun onFailure(call: Call<UserModel>?, t: Throwable?) {
            }

        })
    }

    fun register(registerModel: RegisterModel) {
        apiLoginCall = create().register(registerModel)
        apiLoginCall?.enqueue(object : Callback<UserModel> {
            override fun onFailure(call: Call<UserModel>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<UserModel>?, response: Response<UserModel>?) {
                viewState.login(response?.body() ?: UserModel(false, ""))
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (apiLoginCall?.isExecuted != false)
            apiLoginCall?.cancel()
    }
}

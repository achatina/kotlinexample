package com.kotlinexample.presentation.presenter.products

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kotlinexample.api.ApiProductsService.Factory.create
import com.kotlinexample.api.ProductInfoModel
import com.kotlinexample.presentation.view.products.ProdListView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class ProdListPresenter : MvpPresenter<ProdListView>(){

    private var apiCall: Call<List<ProductInfoModel>>? = null

    fun getProducts(){
        apiCall = create().getProducts()
        apiCall?.enqueue(object : Callback<List<ProductInfoModel>> {
            override fun onFailure(call: Call<List<ProductInfoModel>>?, t: Throwable?) {
                viewState.errorMessage()
                viewState.endProgress()
            }

            override fun onResponse(call: Call<List<ProductInfoModel>>?, response: Response<List<ProductInfoModel>>?) {
                if (response != null)
                    viewState.initAdapter(response.body()!!)
                viewState.endProgress()
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (apiCall?.isExecuted != false)
            apiCall?.cancel()
    }
}
package com.kotlinexample.activities


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.kotlinexample.BuildConfig
import com.kotlinexample.R
import com.kotlinexample.adapters.RatingsAdapter
import com.kotlinexample.api.ProductReviewModel
import com.kotlinexample.api.ProductSingleton
import com.kotlinexample.presentation.presenter.products.DetailProductPresenter
import com.kotlinexample.presentation.view.products.DetailProductView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product.*

class DetailProductActivity : MvpActivity(), DetailProductView {

    private val productSingleton = ProductSingleton

    @InjectPresenter
    lateinit var detailProductPresenter: DetailProductPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product)

        Picasso.with(this).load(BuildConfig.api_url_image + productSingleton.img).into(imageView)

        product_title.text = productSingleton.title
        product_text.text = productSingleton.text

        detailProductPresenter.getRatings(productSingleton.id)

    }

    override fun initAdapter(products: List<ProductReviewModel>) {
        val adapter = RatingsAdapter(products, this)
        recycler_rating.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_rating.adapter = adapter
        recycler_rating.visibility = VISIBLE
    }

    override fun errorMessage() {
        Toast.makeText(this, getString(R.string.detail_product_error), Toast.LENGTH_SHORT).show()
    }

    override fun startProgress() {
        product_progress.visibility = VISIBLE
    }

    override fun endProgress() {
        product_progress.visibility = INVISIBLE
    }
}

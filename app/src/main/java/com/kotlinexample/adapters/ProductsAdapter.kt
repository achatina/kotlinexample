package com.kotlinexample.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kotlinexample.R
import com.kotlinexample.adapters.viewHolders.ProductsViewHolder
import com.kotlinexample.api.ProductInfoModel

class ProductsAdapter(private val products: List<ProductInfoModel>, private val context: Context) : RecyclerView.Adapter<ProductsViewHolder>() {
    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        holder.bindImage(products[position].img)
        holder.bindText(products[position].text, products[position].text)
        holder.bindOnClick(products[position], context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductsViewHolder = ProductsViewHolder(LayoutInflater.from(context).inflate(R.layout.good_item, parent, false))

}
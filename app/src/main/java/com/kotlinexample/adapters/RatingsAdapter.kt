package com.kotlinexample.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kotlinexample.R
import com.kotlinexample.adapters.viewHolders.RatingsViewHolder
import com.kotlinexample.api.ProductReviewModel

class RatingsAdapter(private val products: List<ProductReviewModel>, private val context: Context) : RecyclerView.Adapter<RatingsViewHolder>() {

    override fun onBindViewHolder(holder: RatingsViewHolder, position: Int) {
        holder.bindView(products[position])
    }

    override fun getItemCount(): Int = products.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RatingsViewHolder  = RatingsViewHolder(LayoutInflater.from(context).inflate(R.layout.rating_item, parent, false))

}
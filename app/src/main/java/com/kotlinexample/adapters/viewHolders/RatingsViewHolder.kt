package com.kotlinexample.adapters.viewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.kotlinexample.R
import com.kotlinexample.api.ProductReviewModel

/**
 * Created by Nikita on 29.01.2018.
 */
class RatingsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val v: View = itemView
    private val user: TextView = v.findViewById(R.id.user)
    private val rating: TextView = v.findViewById(R.id.rating)
    private val comment: TextView = v.findViewById(R.id.comment)

    fun bindView(product: ProductReviewModel){
        user.text = product.created_by.username
        rating.text = product.rate.toString()
        comment.text = product.text
    }
}
package com.kotlinexample.adapters.viewHolders

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.kotlinexample.BuildConfig
import com.kotlinexample.R
import com.kotlinexample.activities.DetailProductActivity
import com.kotlinexample.api.ProductInfoModel
import com.kotlinexample.api.ProductSingleton
import com.squareup.picasso.Picasso

/**
 * Created by Nikita on 29.01.2018.
 */
class ProductsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    private val v: View = itemView
    private val img: ImageView = v.findViewById(R.id.product_photo)
    private val title: TextView = v.findViewById(R.id.title)
    private val text: TextView = v.findViewById(R.id.text)

    fun bindImage(url: String){
        Picasso.with(v.context).load(BuildConfig.api_url_image + url).into(img)
    }

    fun bindText(text: String, title: String){
        this.text.text = text
        this.title.text = title
    }

    fun bindOnClick(product: ProductInfoModel, context: Context){
        v.setOnClickListener {
            val productSingleton = ProductSingleton
            productSingleton.id = product.id
            productSingleton.img = product.img
            productSingleton.text = product.text
            productSingleton.title = product.title
            val intent = Intent(context, DetailProductActivity::class.java)
            context.startActivity(intent)
        }
    }
}
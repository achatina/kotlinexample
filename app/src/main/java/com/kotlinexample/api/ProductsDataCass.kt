package com.kotlinexample.api

class ProductInfoModel(val id: Long,
                       val img: String,
                       val text: String,
                       val title: String)

object ProductSingleton {
    var id: Long = 0
    var img: String = ""
    var text: String = ""
    var title: String = ""
}

class ProductReviewModel(val id: Long,
                              val product: Long,
                              val created_by: CreatedByModel,
                              val rate: Int,
                              val text: String)

class CreatedByModel(val id: Long,
                          val username: String)

class PostReviewModel(val rate: Int,
                           val text: String)

class PostReviewResponseModel(val review_id: Long)